package behaviors.quackBehavior;

public interface QuackBehavior {
    public void quack();
}

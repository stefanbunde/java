package behaviors.flyBehavior;

public interface FlyBehavior {
    public void fly();
}

import behaviors.flyBehavior.FlyRocketPowered;
import behaviors.quackBehavior.MuteQuack;
import ducks.Duck;
import ducks.MallardDuck;
import ducks.ModelDuck;
import ducks.RubberDuck;

public class DuckSimulator {
    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        mallard.display();
        mallard.performFly();
        mallard.performQuack();
        mallard.swim();

        System.out.println();

        Duck rubber = new RubberDuck();
        rubber.display();
        rubber.performFly();
        rubber.performQuack();
        rubber.swim();

        System.out.println();

        Duck model = new ModelDuck();
        model.display();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
        model.performQuack();
        model.setQuackBehavior(new MuteQuack());
        model.performQuack();
        model.swim();
    }
}

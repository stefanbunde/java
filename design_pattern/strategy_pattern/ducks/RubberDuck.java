package ducks;

import behaviors.flyBehavior.FlyNoWay;
import behaviors.quackBehavior.Squeak;

public class RubberDuck extends Duck {
    public RubberDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Squeak();
    }

    public void display() {
        System.out.println("I am a rubber duck");
    }
}

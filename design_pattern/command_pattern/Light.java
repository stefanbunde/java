public class Light {
    public Light() {}

    public void on() {
        System.out.println("Light is on now");
    }

    public void off() {
        System.out.println("Light is off now");
    }
}

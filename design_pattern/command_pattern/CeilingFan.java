public class CeilingFan {
    public CeilingFan() {}

    public void on() {
        System.out.println("CeilingFan is on now");
    }

    public void off() {
        System.out.println("CeilingFan is off now");
    }
}

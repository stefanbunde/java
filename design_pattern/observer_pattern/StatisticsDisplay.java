public class StatisticsDisplay implements Observer, DisplayElement {
    private float minTemperature;
    private float maxTemperature;
    private float minPressure;
    private float maxPressure;
    private Subject weatherData;

    public StatisticsDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    public void update(float temperature, float humidity, float pressure) {
        updateTemperature(temperature);
        updatePressure(pressure);
        display();
    }

    private void updateTemperature(float temperature) {
        if (minTemperature == 0 && maxTemperature == 0) {
            setInitialMinAndMaxTemperatures(temperature);
        } else {
            minTemperature = getMinValue(minTemperature, temperature);
            maxTemperature = getMaxValue(maxTemperature, temperature);
        }
    }

    private void setInitialMinAndMaxTemperatures(float temperature) {
        minTemperature = temperature;
        maxTemperature = temperature;
    }

    private void updatePressure(float pressure) {
        if (minPressure == 0 && maxPressure == 0 ) {
            setInitialMinAndMaxPressures(pressure);
        } else {
            minPressure = getMinValue(minPressure, pressure);
            maxPressure = getMaxValue(maxPressure, pressure);
        }
    }

    private void setInitialMinAndMaxPressures(float pressure) {
        minPressure = pressure;
        maxPressure = pressure;
    }

    private float getMinValue(float oldValue, float newValue) {
        if( newValue < oldValue )
            return newValue;
        return oldValue;
    }

    private float getMaxValue(float oldValue, float newValue) {
        if( newValue > oldValue )
            return newValue;
        return oldValue;
    }

    public void display() {
        System.out.println("Min/Max Temperature: " + minTemperature + "/" + maxTemperature +
                           "°C degrees and Min/Max Pressure: " + minPressure + "/" + maxPressure);
    }
}

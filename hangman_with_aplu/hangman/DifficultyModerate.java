package hangman;

import hangman.Hangturtle;

public class DifficultyModerate implements Difficulty {
    public boolean isGameLost(int failures) {
        return failures >= 8;
    }

    public void failure(Hangturtle turtle, int failures) {
        turtle.forward(40);
        turtle.label(failures);

        switch(failures) {
            case 3:
            case 6:
                turtle.right();
                break;
            case 8:
                turtle.right();
                turtle.right();
                break;
            default:
                break;
        }
    }
}

package hangman;

import java.util.HashSet;

public class Letters {
    private HashSet letters;

    public Letters() {
        this.letters = new HashSet();
    }

    public void addLetter(char letter) {
        this.letters.add(letter);
    }

    public boolean wasLetterAlreadyEntered(char letter) {
        return this.letters.contains(letter);
    }

    public void clear() {
        this.letters.clear();
    }
}

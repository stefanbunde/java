package hangman;

import hangman.Hangturtle;

public class DifficultyEasy implements Difficulty {
    public boolean isGameLost(int failures) {
        return failures >= 16;
    }

    public void failure(Hangturtle turtle, int failures) {
        turtle.forward(25);
        turtle.label(failures);

        switch(failures) {
            case 6:
            case 12:
                turtle.right();
                break;
            case 16:
                turtle.right();
                turtle.right();
                break;
            default:
                break;
        }
    }
}

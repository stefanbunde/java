package hangman;

import java.awt.Color;

import ch.aplu.turtle.*;

public class Hangturtle {
    Turtle turtle = new Turtle();
    int rightHandRotations;

    public void init() {
        turtle.clear();
        turtle.home();
        turtle.setPenColor(Color.BLUE);
        turtle.setColor(Color.blue);
        turtle.setFontSize(12);
        turtle.setPos(-95.0, -70.0);
        rightHandRotations = 0;
    }

    public void forward(int width) {
        turtle.forward(width);
    }

    public void right() {
        turtle.right(90);
        rightHandRotations++;

        if (rightHandRotations == 1) {
            turtle.setPenColor(Color.MAGENTA);
        } else if (rightHandRotations == 2) {
            turtle.setPenColor(Color.RED);
        }
    }

    public void label(int failures) {
        turtle.label(Integer.toString(failures));
    }
}

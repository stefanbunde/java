package hangman;

import hangman.Hangturtle;

public interface Difficulty {
    public boolean isGameLost(int failures);

    public void failure(Hangturtle turtle, int failures);
}

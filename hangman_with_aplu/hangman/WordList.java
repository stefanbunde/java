package hangman;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Random;

public class WordList {
    static String[] WORDS = {
        "alphabet", "hochbett", "quadratmeter", "wasserbett"
    };
    private Random random = new Random();
    private ArrayList<String> words;

    public WordList() {
        words = new ArrayList<String>(Arrays.asList(WORDS));
    }

    public int count() {
        return words.size();
    }

    public String popRandomWord() {
        int index = random.nextInt(count());
        return words.remove(index);
    }
}

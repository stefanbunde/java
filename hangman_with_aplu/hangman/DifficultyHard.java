package hangman;

import hangman.Hangturtle;

public class DifficultyHard implements Difficulty {
    public boolean isGameLost(int failures) {
        return failures >= 4;
    }

    public void failure(Hangturtle turtle, int failures) {
        turtle.forward(60);
        turtle.label(failures);

        switch(failures) {
            case 2:
            case 3:
                turtle.right();
                break;
            case 4:
                turtle.right();
                turtle.right();
                break;
            default:
                break;
        }
    }
}

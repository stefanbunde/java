package io;

import java.util.Scanner;

import hangman.Difficulty;
import hangman.DifficultyEasy;
import hangman.DifficultyModerate;
import hangman.DifficultyHard;

public class Decision {
    private Scanner in = new Scanner(System.in);

    public boolean promptPlayAgain() {
        String input;

        System.out.println("Do you want to play again? y/n");

        while (!in.hasNext("[yYnN]")) {
            System.out.println("Invalid input! Please enter 'y' or 'n'");
            input = in.next();
        }
        input = in.next();

        if (input.toLowerCase().equals("y")) {
            return true;
        }
        return false;
    }

    public char promptLetter() {
        System.out.print("Choose a letter: ");

        String input = in.next();
        while (input.length() > 1) {
            System.out.println("Too many letters. Please enter just a single one!");
            input = in.next();
        }
        return input.charAt(0);
    }

    public Difficulty promptLevelOfDifficulty() {
        System.out.println("Choose a level of difficulty! 1-easy, 2-moderate, 3-hard");

        switch (readLevelOfDifficulty()) {
            case 1: return new DifficultyEasy();
            case 2: return new DifficultyModerate();
            case 3: return new DifficultyHard();
            default: return new DifficultyEasy();
        }
    }

    private int readLevelOfDifficulty() {
        int input = readInt();
        while (input < 1 || input > 3) {
            System.out.println("Invalid value! 1-easy, 2-moderate, 3-hard");
            input = readInt();
        }
        return input;
    }

    private int readInt() {
        while (!in.hasNextInt()) {
            System.out.println("That's not a number!");
            in.next();
        }
        return in.nextInt();
    }
}

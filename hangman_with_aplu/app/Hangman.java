package app;

import hangman.Difficulty;
import hangman.Hangturtle;
import hangman.Letters;
import hangman.WordList;
import io.Decision;

public class Hangman {
    Decision decision = new Decision();
    Difficulty difficulty;
    Hangturtle turtle = new Hangturtle();
    Letters letters = new Letters();
    WordList words = new WordList();
    int failures, guessCount;
    String originalWord;

    public void start() {
        char letter;
        StringBuffer word;

        System.out.println("Hangman started");

        do {
            init();
            word = initializeWordWithUnderscores(originalWord);
            while (word.indexOf("_") != -1) {
                System.out.println(word.toString());

                letter = readLetter();
                word = setLettersInWord(word, letter);
                if (difficulty.isGameLost(failures)) {
                    System.out.println("You lost!");
                    break;
                }
            }
            if (!difficulty.isGameLost(failures)) {
                System.out.println(String.format("Solved with %d guesses", guessCount));
            }
            System.out.println(String.format("Original word: %s", originalWord));
        } while (words.count() > 0 && decision.promptPlayAgain());

        if (words.count() == 0) {
            System.out.println("Congratulation. You finished.");
        }

        System.out.println("Quit game");
    }

    private void init() {
        turtle.init();
        difficulty = decision.promptLevelOfDifficulty();
        failures = 0;
        guessCount = 0;
        letters.clear();
        originalWord = words.popRandomWord();
    }

    private StringBuffer initializeWordWithUnderscores(String word) {
        StringBuffer sb = new StringBuffer();
        word.chars().forEach(letter -> sb.append("_"));
        return sb;
    }

    private char readLetter() {
        char letter = decision.promptLetter();

        while (letters.wasLetterAlreadyEntered(letter)) {
            System.out.println("This letter was already entered");
            letter = decision.promptLetter();
        }

        guessCount++;
        letters.addLetter(letter);
        return letter;
    }

    private StringBuffer setLettersInWord(StringBuffer word, Character letter) {
        int position = originalWord.indexOf(letter.toString());

        if (position == -1) {
            System.out.println(String.format("The word doesn't contains the letter '%s'!", letter));
            failures++;
            difficulty.failure(turtle, failures);
        }

        while (position != -1) {
            word.setCharAt(position, letter);
            position = originalWord.indexOf(letter.toString(), position + 1);
        }
        return word;
    }

    public static void main(String[] args) {
        (new Hangman()).start();
    }
}
